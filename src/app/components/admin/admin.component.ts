import { Component, OnInit } from '@angular/core';
import { PropiedadService } from '../../propiedad.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  public propiedades: any = [];

  constructor(private propiedadesService: PropiedadService) {}

  ngOnInit() {
    this.propiedadesService.listaPropiedades().subscribe(
      (data) => {
        this.propiedades = data;
      },
      (err) => {
        console.log(err);
      },
      () => {});
  }
}
