import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from '@angular/common/http';
import { environment } from "../environments/environment" ;
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PropiedadService {

  endpoint  = environment.api_endpoint;

  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': localStorage.getItem('ACCESS_TOKEN')
    })
  };

  constructor(private http: HttpClient) { }

  listaPropiedades() {
    let propiedades =  this.http.get(this.endpoint + 'propiedades/', this.httpOptions);
    return propiedades;
  }

}
