import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from '@angular/common/http';
import { environment } from "../environments/environment" ;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  endpoint  = environment.api_endpoint;

  constructor(private http: HttpClient) { }

  public login(userInfo) {
    
    const body = {
      username: userInfo.username,
      password : userInfo.password
    };
    const login =  this.http.post(this.endpoint + 'login', body );
    return login;
  }

  public isLoggedIn() {
    return localStorage.getItem('LOGGED') !== null;

  }

  public logout() {
    localStorage.removeItem('ACCESS_TOKEN');
    localStorage.removeItem('LOGGED');

  }
}
